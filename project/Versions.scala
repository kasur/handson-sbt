object Versions {
  val Akka = "2.5.11"
  val Logback = "1.1.2"
  val Akka_Stream = "2.5.11"
  val Macwire = "2.3.0"
  val Scalatest = "3.0.5"
}