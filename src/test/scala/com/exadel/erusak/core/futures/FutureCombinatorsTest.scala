package com.exadel.erusak.core.futures

import org.scalatest.{FunSpec, Matchers}

import scala.concurrent.Await

import com.softwaremill.macwire._

/**
  * Created by kasured on 6/22/16.
  */
class FutureCombinatorsTest extends FunSpec with Matchers with ConcurrentUtils {

  describe("Futures") {
    it("could be composed using map") {

      //implicit imported into the scope to allow "seconds"
      import scala.concurrent.duration.DurationInt

      lazy val futureCombinators = wire[FutureCombinators]
      val result = timed {
        Await.result(futureCombinators.sumOfFreeNumbersSequentialMap(), 7 seconds)
      }
      result shouldBe 6
    }
    it("could be composed using for comprehension") {
      //implicit imported into the scope to allow "seconds"
      import scala.concurrent.duration.DurationInt

      val futureCombinators = new FutureCombinators
      val result = timed {
        Await.result(futureCombinators.sumOfFreeNumbersForComprehension(), 7 seconds)
      }
      result shouldBe 6
    }
    it("could be composed using parallel approach") {
      //implicit imported into the scope to allow "seconds"
      import scala.concurrent.duration.DurationInt

      val futureCombinators = new FutureCombinators

      val result = timed {
        Await.result(futureCombinators.sumOfFreeNumbersParallel(), 7 seconds)
      }
      result shouldBe 6
    }
  }
}
