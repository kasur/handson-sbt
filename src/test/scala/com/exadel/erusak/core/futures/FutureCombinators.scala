package com.exadel.erusak.core.futures

import scala.concurrent.Future

/**
  * Created by kasured on 6/22/16.
  */
class FutureCombinators {

  def sumOfFreeNumbersSequentialMap(): Future[Int] = {
    Future {
      Thread sleep 1000
      1
    } flatMap { oneValue =>
      Future {
        Thread sleep 2000
        2
      } flatMap { twoValue =>
        Future {
          Thread sleep 3000
          3
        } map { thirdValue =>
            oneValue + twoValue + thirdValue
        }
      }
    }
  }

  def sumOfFreeNumbersForComprehension(): Future[Int] = {
    for {
      one <- Future {
        Thread sleep 1000
        1
      }
      two <- Future {
        Thread sleep 2000
         2
      }
      three <- Future {
        Thread sleep 3000
        3
      }
    } yield one + two + three
  }

  def sumOfFreeNumbersParallel(): Future[Int] = {

    val oneF = Future {
      Thread sleep 1000
      1
    }

    val twoF = Future {
      Thread sleep 2000
      2
    }

    val threeF = Future {
      Thread sleep 3000
      3
    }

    for {
      one <- oneF
      two <- twoF
      three <- threeF
    } yield one + two + three
  }
}