package com.exadel.erusak.core.futures

/**
  * Created by kasured on 6/22/16.
  */
trait ConcurrentUtils {
  def timed[T](block: => T): T = {
    val start  = System.currentTimeMillis()
    val result = block
    val duration = System.currentTimeMillis() - start

    println(s"time taken: ${duration}ms")

    result
  }
}
