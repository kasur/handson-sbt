package com.exadel.erusak.core.scalatest

import org.scalatest.{MustMatchers => Must, FlatSpec}

/**
 * @author erusak.
 */
class BDDSuite extends FlatSpec with Must {
  "An empty Set" should "have size 0" in {
    Set.empty.size must be (0)
  }

  it should "throw exception NoSuchElement if head method is called on it" in {
    an [NoSuchElementException] must be thrownBy {
      Set.empty.head
    }
  }
}
