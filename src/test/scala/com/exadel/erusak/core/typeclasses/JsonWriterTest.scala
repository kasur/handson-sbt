package com.exadel.erusak.core.typeclasses

import org.scalatest.{FeatureSpec, GivenWhenThen, MustMatchers}

/**
  * Created by erusak@exadel.com on 4/26/18.
  */
class JsonWriterTest extends FeatureSpec with GivenWhenThen with MustMatchers {

  val testPerson = Person(name = "erusak", email = "erusak@exadel.com")
  val testJsonPerson = JsonObject(Map("name" -> JsonString(testPerson.name), "email" -> JsonString(testPerson.email)))
  val testJsonString = JsonString("I am just a string")

  val somePerson = Option(Person(name = "john doe", email = "john.doe@google.com"))
  val someJsonPerson = JsonObject(Map("name" -> JsonString(somePerson.get.name), "email" -> JsonString(somePerson.get.email)))
  val testJsonArray = JsonArray(List(someJsonPerson, someJsonPerson))

  feature("Type classes") {

    scenario("Implicit Object Api") {

      Given("Implicit Object Api approach")

      Then("we should be able to use it")
      JsonApi.toJson(testJsonString.value) mustBe testJsonString
      JsonApi.toJson(testPerson) mustBe testJsonPerson
      JsonApi.toJson(somePerson) mustBe someJsonPerson
      JsonApi.toJson(Option.empty[String]) mustBe JsonNull
      JsonApi.toJson(List(somePerson, somePerson)) mustBe JsonArray(List(someJsonPerson, someJsonPerson))
    }

    scenario("Implicit Syntax Api") {
      Given("Implicit Syntax Api available in scope")

      Then("we can use it as well in a different manner")
      testJsonString.value.toJson mustBe testJsonString
      testPerson.toJson mustBe testJsonPerson
      somePerson.toJson mustBe someJsonPerson
      Option.empty[String].toJson mustBe JsonNull
      List(somePerson, somePerson).toJson mustBe testJsonArray
    }



  }


}
