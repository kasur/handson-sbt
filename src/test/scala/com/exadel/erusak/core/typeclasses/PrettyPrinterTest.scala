package com.exadel.erusak.core.typeclasses

import org.scalatest.{FunSuite, MustMatchers}

/**
  * Created by kasured on 3/21/17.
  */
class PrettyPrinterTest extends FunSuite with MustMatchers {
  test("Output test for pretty printer") {
    import PrettyPrinter._
    prettyPrintContextBound(List(1,2,3)) must be ("1||2||3")
    prettyPrintWithImplicit(List(1,2,3)) must be ("1||2||3")
  }
}