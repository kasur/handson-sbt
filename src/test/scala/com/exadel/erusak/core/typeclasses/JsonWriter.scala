package com.exadel.erusak.core.typeclasses

/**
  * Created by erusak@exadel.com on 4/26/18.
  */
trait JsonWriter[A] {
  def write(value: A): Json
}

private[typeclasses] object JsonWriter {

  implicit object StringWriter extends JsonWriter[String] {
    override def write(value: String): Json = JsonString(value)
  }

  implicit def optionWriter[A](implicit valueWriter: JsonWriter[A]): JsonWriter[Option[A]] =
    (optValue: Option[A]) => optValue match {
      case Some(value) => valueWriter.write(value)
      case None => JsonNull
    }

  implicit def listWriter[A](implicit valueWriter: JsonWriter[A]): JsonWriter[List[A]] =
    (collection: List[A]) => JsonArray(collection.map(valueWriter.write))
}
