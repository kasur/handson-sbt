package com.exadel.erusak.core.typeclasses

/**
  * Created by kasured on 3/21/17.
  */
trait PrettyPrinter[A] {
  def encode(value: A): String
}

object PrettyPrinter {
  implicit object ListPrettyPrinter extends PrettyPrinter[List[Int]] {
    override def encode(value: List[Int]): String = value map(v => v.toString) mkString "||"
  }
}
