package com.exadel.erusak.core.typeclasses

/**
  * Created by erusak@exadel.com on 4/26/18.
  */
private[typeclasses] sealed trait Json

case class JsonArray(value: List[Json]) extends Json
case class JsonObject(value: Map[String, Json]) extends Json
case class JsonString(value: String) extends Json
case class JsonNumber(value: Double) extends Json

case object JsonNull extends Json
