package com.exadel.erusak.core.typeclasses

/**
  * Created by erusak@exadel.com on 4/26/18.
  */
object JsonApi {
  def toJson[A](value: A)(implicit jsonWriter: JsonWriter[A]): Json = {
    jsonWriter.write(value)
  }
}
