package com.exadel.erusak.core.typeclasses

/**
  * Created by erusak@exadel.com on 4/26/18.
  */
private[typeclasses] case class Person(name: String, email: String)

private[typeclasses] object Person {
  implicit object PersonWriter extends JsonWriter[Person] {
    override def write(person: Person): Json = JsonObject(
      Map(
        "name" -> JsonString(person.name),
        "email" -> JsonString(person.email)
      )
    )
  }
}
