package com.exadel.erusak.core

/**
  * Created by kasured on 3/21/17.
  */
package object typeclasses {
  def prettyPrintContextBound[A : PrettyPrinter](value: A): String = {
    implicitly[PrettyPrinter[A]].encode(value)
  }
  def prettyPrintWithImplicit[A](value: A)(implicit printer: PrettyPrinter[A]): String = {
    printer.encode(value)
  }

  implicit class JsonWriterOps[A](value: A) {
    def toJson(implicit jsonWriter: JsonWriter[A]): Json = jsonWriter.write(value)
  }

}
