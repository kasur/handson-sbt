package com.exadel.erusak.core.types.typeclass

import org.scalatest.{FlatSpec, MustMatchers}

/**
 * @author erusak.
 */
class StatisticsTestSuite extends FlatSpec with MustMatchers {
  "Testing statistics API" should "pass smoothly" in {

    val vectorOfDoubles = Vector[Double](1, 2, 3, 4, 5, 5.3)
    val vectorOfInts = Vector[Int](1, 2, 3, 4, 5, 7, 9)

    //mean
    println(Statistics.mean(vectorOfDoubles))

    //median
    println(Statistics.median(vectorOfInts))

    //quartiles
    println(Statistics.quartiles(vectorOfInts))
  }
}
