package com.exadel.erusak.core.collections

import org.scalatest.{MustMatchers, FlatSpec}

/**
 * @author erusak.
 */
class CoreTestSuite extends FlatSpec with MustMatchers {
  "Collection reduce with given op" should "behave as expected against the elements of seq" in {
    val seq = Seq[Int](1, 2, 3, 4, 5)

    val op: (Int, Int) => Int = _ + _

    seq reduce op must be (15)

    seq reduce (_ * _) must be (120)

  }
}
