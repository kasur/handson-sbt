package com.exadel.erusak.core.maths

import org.scalatest.{FlatSpec, Matchers, MustMatchers}

/**
  * Created by kasured on 7/26/16.
  */
class Misc extends FlatSpec with MustMatchers {

  import com.exadel.erusak.core.maths.Misc._

  info {
    """Exploring Some Simple Math algorithms
    """.stripMargin
  }

  "abcdf" isPermutationOf "afbdc" mustBe true

  "abcdf" isPermutationOf "afkdc" mustBe false

  "abcdf" isPermutationOf "afkc" mustBe false

  "jsfdekpw" isPermutationOf "pdjsfwke" mustBe true

}


object Misc {
  implicit class StringEnhancement(str: String) {
    def isPermutationOf(anotherStr: String): Boolean = {
      if (str.length != anotherStr.length) false
      else {
        val indicator = Array.ofDim[Int](255)
        0 until str.length foreach(idx => {
          val charIdxOfStr = str.charAt(idx).toInt
          val charIdxOfAnotherStr = anotherStr.charAt(idx).toInt
          indicator(charIdxOfStr) = indicator(charIdxOfStr) + 1
          indicator(charIdxOfAnotherStr) = indicator(charIdxOfAnotherStr) - 1
        })
        indicator collectFirst { case x: Int if x != 0 => false } getOrElse true
      }
    }
  }
}