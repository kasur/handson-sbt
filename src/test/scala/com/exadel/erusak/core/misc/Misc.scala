package com.exadel.erusak.core.misc

import org.scalatest.{FlatSpec, Matchers, MustMatchers}

/**
 * @author kasured.
 */
class Misc extends FlatSpec with MustMatchers {

  object Singleton {
    def aa = "aa"
  }

  val o1 = Singleton
  val o2 = Singleton

  o1 eq o2 mustBe (right = true)

  def lambda = new Function1[Int, Int] {
    override def apply(v1: Int): Int = v1 + 1
  }

  def mult(f: (Int) => Int): (Int) => Int = {
    (x: Int) => 7 * f(x)
  }

  lambda(5) must be (6)
  mult(lambda) {5} must be (42)

  val map = Map("one" -> 1, "two" -> 2, "three" -> 3)
  map("one") must be (1)
  map get "two" mustBe Some(2)
  map get "four" mustBe None

  val anotherMap = Map("one" -> 1, 1 -> 2) //type inference happens here cause we o not provide it explicitly

  // backquotes and Variable shadowing problem
  val variable = "one"
  val matchResult: (String) => String = {
    case `variable` => "match"
    case _ => "oops"
  }
  matchResult("one") must be ("match")
  matchResult("two") mustBe "oops"

  //partial function testing
  val even: PartialFunction[Int, String] = {
    case x if x % 2 == 0 => s"$x is even"
  }
  val ring: PartialFunction[Int, String] = even orElse { case x => s"$x is odd"}

  (even isDefinedAt 10) mustBe true
  (even isDefinedAt 11) mustBe false
  (ring isDefinedAt 11) mustBe true

  even(10) must be ("10 is even")
  ring(11) must be ("11 is odd")

  behavior of "List operations"

  it should "fold to left and calculate a sum" in {
    val result = (0 /: List(1,2,3,4,5)) {
      (`total sum`, `next element`) => `total sum` + `next element`
    }

    result mustBe 15

  }

}
