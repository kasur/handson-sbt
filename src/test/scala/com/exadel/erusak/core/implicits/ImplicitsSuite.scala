package com.exadel.erusak.core.implicits

import org.scalatest.{FlatSpec, Matchers, MustMatchers}

/**
 * @author kasured.
 */
class ImplicitsSuite extends FlatSpec with MustMatchers {

  behavior of "Implicits must be as expected"

  it should "take the implicit import and apply to the argument" in {

    import Implicits._

    11.isEven mustBe false
    10.isOdd must be (false)

    17.isOdd mustBe true
    12.isEven mustBe true

  }


}

object Implicits {
  implicit class Evenness(val original: Int) {
    def isEven = original % 2 == 0
    def isOdd = !isEven
  }
}
