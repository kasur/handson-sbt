package com.exadel.erusak.core

import scala.collection.mutable.ArrayBuffer

/**
  * Created by kasured on 11/25/16.
  */
package object traits {

  abstract class IntQueue {

    def get: Int

    def put(x: Int): Unit
  }

  class BasicQueue extends IntQueue {

    private val buffer = new ArrayBuffer[Int]

    override def get: Int = buffer.remove(0)

    override def put(x: Int): Unit = buffer += x
  }

  trait Doubled extends IntQueue {
    abstract override def put(x: Int): Unit = super.put(x * 2)
  }

  trait Filter extends IntQueue {
    abstract override def put(x: Int) = if (x >= 0) super.put(x)
  }

}
