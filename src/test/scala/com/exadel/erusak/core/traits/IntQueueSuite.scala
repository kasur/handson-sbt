package com.exadel.erusak.core.traits

import org.scalatest.FunSuite

/**
  * Created by kasured on 11/25/16.
  */
class IntQueueSuite extends FunSuite {
  test("Testing the stackable queue") {
    val doubledFilter = new BasicQueue with Filter with Doubled
    val filterDoubled = new BasicQueue with Doubled with Filter

    doubledFilter.put(1)
    doubledFilter.put(-1)

    assert(doubledFilter.get === 2)

    intercept[IndexOutOfBoundsException] {
      doubledFilter.get
    }

  }
}
