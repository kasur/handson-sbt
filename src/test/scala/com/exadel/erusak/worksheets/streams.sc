val fibs:Stream[Int] = {
  def _fib(a: Int, b: Int): Stream[Int] = a #:: _fib(b, a + b)
  _fib(0,1)
}

fibs drop 3 take 5 toList

fibs
