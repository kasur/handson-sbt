import scala.collection.LinearSeq

sealed trait BinaryTree[+A]

case object NilTree extends BinaryTree[Nothing]

case class Leaf[+A](value: A) extends BinaryTree[A]

case class Branch[+A](value: A, lht: BinaryTree[A], rht: BinaryTree[A]) extends BinaryTree[A]

def traverse[A, U](tree: BinaryTree[A])(f: (A) => U): Unit = {
  def _traverse(node: BinaryTree[A], stack: LinearSeq[BinaryTree[A]]): Unit = node match {
    case Branch(value, lht, rht) =>
      f(value)
      _traverse(lht, rht +: stack)
    case Leaf(value) if stack.nonEmpty =>
      f(value)
      _traverse(stack.head, stack.tail)
    case Leaf(value) =>
      f(value)
      ()
    case NilTree if stack.nonEmpty =>
      _traverse(stack.head, stack.tail)
    case NilTree =>
      ()
  }
  _traverse(tree, LinearSeq())
}

val tree = Branch[Int](1, Leaf(2), Branch(5, Leaf(4), Leaf(8)))

traverse(tree){
  println
}
